/**
 * Any code used inside this helper is ignored by Handlebars. Use it if your email service provider uses a Handlebars-like syntax.
 * @example
 * <img src="{{base64}}/assets/img/logo.png{{/base64}}">
 */
var fs = require('fs');
var path = require('path');

function replaceToBase64 (content, fd) {
  var getFileExtension = function (file) {
        var ext = 'png';

        if (file.indexOf("png") !== -1) {
          ext = "png"
        } else if (file.indexOf("jpg") !== -1 || file.indexOf("jpeg") !== -1) {
          ext = "jpg"
        } else {
          ext = "tiff";
        }
      },
      ext = getFileExtension(content.toLowerCase()),
      data = 'data:image/' + ext + ';base64,' + Buffer.from(fd).toString('base64');

  return data;


}

module.exports = function(str) {

    var file = path.resolve('src/' + str.fn(this));

    fs.readFile(file, function (err, data) {
      if (err) throw err;

      return replaceToBase64(file, data);
    });


  //return '';

  // if (str && typeof str === 'string') {
  //
  //   fs.readFile(content.fn(this), function (err, data) {
  //     if (err) throw err;
  //
  //     return replaceToBase64(content.fn(), data);
  //   });
  //
  // }



}
