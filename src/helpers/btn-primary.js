/**
 * Generuje przycisk
 * @example
 *
 * {{#btn classmname}}Tekst przycisku{{/btn}
 */
module.exports = function(context) {
  var html = '',
      startCode = '',
      endCode = '',
      link = '',
      strong = '';

   var href = context.hash.href;
    var height = context.hash.height;
    var width = context.hash.width;

  startCode = '<!--[if mso]>'+
      '<v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="'+ href +'" style="height:'+height+';v-text-anchor:middle;width:'+width+';" arcsize="12%" stroke="f" fillcolor="#ef5236">'+
      '<w:anchorlock/>'+
  '<center>'+
  '<![endif]-->';

  strong = '<strong style="font-size: 16px; color: #ffffff;">' + context.fn(this) + ' »</strong>';

  link = '<a class="btn btn-primary btn-large" href="' + href + '"'+
  'style="background-color:#ef5236;background: #ef5236; color: #fefefe; background: -moz-linear-gradient(top, #f38780 0%, #f5654c 4%, #e53d16 95%, #cc3614 100%); background: -webkit-linear-gradient(top, #f38780 0%, #f5654c 4%, #e53d16 95%, #cc3614 100%); background: linear-gradient(to bottom, #f38780 0%, #f5654c 4%, #e53d16 95%, #cc3614 100%);border-radius:4px;color:#fefefe;display:inline-block;font-family:sans-serif;font-size:16px;font-weight:bold;line-height:' + height + ';text-align:center;text-decoration:none;width:' + width + ';-webkit-text-size-adjust:none;">' + strong + '</a>';


  endCode = '<!--[if mso]>'+
      '</center>'+
      '</v:roundrect>'+
  '<![endif]-->';

  html = startCode + link + endCode;

  return html;
}
